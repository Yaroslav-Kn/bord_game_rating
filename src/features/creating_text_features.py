from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.corpus import stopwords as nltk_stopwords
import click
from save_load_sparse_csr import save_sparse_csr

nltk.download('stopwords')
stopwords = nltk_stopwords.words('english')


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
@click.argument('max_ngram_range', type=click.INT)
@click.argument('max_ftr', type=click.INT)
def creating_text_features(input_path: str, output_path: str,
                           max_ngram_range: int, max_ftr: int):
    """Function for creating sparce matrix with TfidfVectorizer
    :param input_path: Path to read text corpus
    :param output_path: Path to write sparse matrix (.npz)
    :param max_ngram_range: max length ngtam
    :param max_ftr: max features
    :return:
    """
    tf = TfidfVectorizer(ngram_range=(1, max_ngram_range),
                         max_features=max_ftr,
                         stop_words=stopwords)
    with open(input_path) as inp_train_file:
        X = tf.fit_transform(inp_train_file)
    save_sparse_csr(output_path, X.tocsr())


if __name__ == "__main__":
    creating_text_features()
