import pandas as pd
import click


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path_target", type=click.Path())
@click.argument("output_path_features", type=click.Path())
def separate_target_features(input_path: str, output_path_target: str,
                  output_path_features: str):
    """Function for separate target
    :param input_path: Path to read original DataFrame
    :param output_path_target: Path to write target DataFrame
    :param output_path_features: Path to write features DataFrame
    :return:
    """
    df = pd.read_csv(input_path)
    df_target = df[['Rating Average']]
    df_features = df.drop(['Rating Average'], axis=1)

    df_target.to_csv(output_path_target, index=False)
    df_features.to_csv(output_path_features, index=False)


if __name__ == "__main__":
    separate_target_features()
