import mlflow
from mlflow.models.signature import infer_signature
from mlflow import MlflowClient
import click
import joblib as jb
import json
import os
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import pandas as pd
from save_load_sparse_csr import load_sparse_csr

RANDOM_STATE = 123

mlflow.set_tracking_uri('http://83.166.240.189:5000')
os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://83.166.240.189:9000'


@click.command()
@click.argument('input_features', type=click.Path(exists=True), nargs=2)
@click.argument('input_target', type=click.Path(exists=True), nargs=2)
@click.argument('output_path', type=click.Path(), nargs=2)
def train_model(input_features: list[str],
                input_target: list[str],
                output_path: list[str]):
    """
    Function for train model
    :param input_features: list of path to read npz matrix of features
    :param input_target: list of path to read target
    :param output_path: list of path to write
    :return:
    """
    mlflow.sklearn.autolog()
    with mlflow.start_run():
        X_train = load_sparse_csr(input_features[0])
        X_test = load_sparse_csr(input_features[1])
        y_train = pd.read_csv(input_target[0])['Rating Average']
        y_test = pd.read_csv(input_target[1])['Rating Average']

        model = RandomForestRegressor(n_estimators=500, max_depth=6)
        model.fit(X_train, y_train)

        os.makedirs(os.path.dirname(output_path[0]), exist_ok=True)
        jb.dump(model, output_path[0])

        y_predicted = model.predict(X_test)

        score = dict(
            mae=mean_absolute_error(y_test, y_predicted),
            rmse=mean_squared_error(y_test, y_predicted)
        )

        with open(output_path[1], 'w') as score_file:
            json.dump(score, score_file, indent=4)

        signature = infer_signature(X_train, y_predicted)
        mlflow.sklearn.log_model(sk_model=model,
                                 artifact_path='model',
                                 signature=signature)
        autolog_run = mlflow.last_active_run()


if __name__ == "__main__":
    train_model()


