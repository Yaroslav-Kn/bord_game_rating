import pandas as pd
import click

OPT_MIN_PLAYERS = 8
OPT_MAX_PLAYERS = 12
OPT_PLAY_TIME = 400


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def data_cleaning(input_path: str, output_path: str):
    """Function for filtering training DataFrame
    :param input_path: Path to read original DataFrame
    :param output_path: Path to read processed DataFrame
    :return:
    """
    df = pd.read_csv(input_path)
    df = df[
        (df["Year Published"] != 0)
        & (df["Min Players"] != 0)
        & (df["Max Players"] != 0)
        & (df["Play Time"] != 0)
        & (df["Year Published"] != 0)
    ]

    df = df[
        (df["Min Players"] <= OPT_MIN_PLAYERS)
        & (df["Max Players"] != OPT_MAX_PLAYERS)
        & (df["Play Time"] != OPT_PLAY_TIME)
    ]

    df = df.drop(["ID",
                  "Name",
                  "Users Rated",
                  "BGG Rank",
                  'Year Published',
                  'Owned Users'], axis=1)
    df = df[(df["Complexity Average"] != 0)]

    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    data_cleaning()
