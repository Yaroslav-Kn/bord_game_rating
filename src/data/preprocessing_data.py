import pandas as pd
import click


def cast_data_types(input_df: pd.DataFrame,
                    is_train_df: bool = True) -> pd.DataFrame:
    """Function to cast data types
    :param input_df: original DataFrame
    :param is_train_df: flag of DataFrame type
    :return : cast DataFrame
    """
    input_df["Rating Average"] = (
        input_df["Rating Average"].str.replace(",", ".").astype("float")
    )
    input_df["Complexity Average"] = (
        input_df["Complexity Average"].str.replace(",", ".").astype("float")
    )

    if is_train_df:
        input_df = input_df.dropna(subset=["Year Published", "Owned Users"])
    else:
        input_df["Year Published"].fillna(2021, inplace=True)
        input_df["Owned Users"].fillna(0, inplace=True)
    input_df["Year Published"] = input_df["Year Published"].astype("int")
    input_df["Owned Users"] = input_df["Owned Users"].astype("int")
    return input_df


def get_popular_periods(input_df: pd.DataFrame) -> pd.DataFrame:
    """Function  to highlight popularity by release year
    :param input_df: original DataFrame
    :return: cast DataFrame
    """
    df = input_df.copy()
    df['not_popular'] = df['Year Published'] <= 1950
    df['less_popular'] = ((df['Year Published'] > 1950) &
                          (df['Year Published'] <= 1970))
    df['gaining_popular'] = ((df['Year Published'] > 1970) &
                             (df['Year Published'] <= 2000))
    df['increasing_popular'] = ((df['Year Published'] > 2000) &
                                (df['Year Published'] <= 2015))
    df['very_popular'] = df['Year Published'] >= 2015
    df[['not_popular',
        'less_popular',
        'gaining_popular',
        'increasing_popular',
        'very_popular']] = df[['not_popular',
                               'less_popular',
                               'gaining_popular',
                               'increasing_popular',
                               'very_popular']].astype(int)
    return df


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
@click.argument("is_train_df", type=click.BOOL)
def preprocessing_data(
    input_path: str, output_path: str, is_train_df: bool = True
):
    """Function for code preprocessing
    :param input_path: Path to read original DataFrame
    :param output_path: Path to read processed DataFrame
    :param is_train_df: flag of DataFrame type
    """
    df = pd.read_csv(input_path, sep=";")
    df = cast_data_types(df, is_train_df)
    df = get_popular_periods(df)

    df["Mechanics"] = df["Mechanics"].fillna("")
    df["Domains"] = df["Domains"].fillna("")

    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    preprocessing_data()
