import numpy as np
import scipy


def save_sparse_csr(filename: str, array):
    np.savez(filename, data=array.data, indices=array.indices,
             indptr=array.indptr, shape=array.shape)


def load_sparse_csr(filename: str):
    loader = np.load(filename)
    return scipy.sparse.csr_matrix((loader['data'],
                                    loader['indices'],
                                    loader['indptr']),
                                   shape=loader['shape'])
