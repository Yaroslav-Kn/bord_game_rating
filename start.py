import src

RAW_DATA_PATH = 'data/raw/bgg_dataset.csv'
PREPROCESSING_DATA_PATH = 'data/interim/data_preproc.csv'
CLEANED_DATA_PATH = 'data/interim/data_cleaned.csv'
TARGET_DATA_PATH = 'data/processed/target.csv'
CLEANED_FEATURES_PATH = 'data/interim/features_cleaned.csv'
TEXT_CORPUS_PATH = 'data/interim/text_corpus.txt'
TEXT_MATRIX_PATH = 'data/interim/text_matrix.npz'
FINISH_FEATURES = 'data/processed/features.npz'
MAX_NGRAMM = 2
MAX_FEATURES = 10000


if __name__ == '__main__':
    src.preprocessing_data(RAW_DATA_PATH,
                           PREPROCESSING_DATA_PATH)
    src.data_cleaning(PREPROCESSING_DATA_PATH,
                      CLEANED_DATA_PATH)
    src.separate_target_features(CLEANED_DATA_PATH,
                                 TARGET_DATA_PATH,
                                 CLEANED_FEATURES_PATH)
    src.creating_text_corpus(CLEANED_FEATURES_PATH,
                             TEXT_CORPUS_PATH)
    src.creating_text_features(TEXT_CORPUS_PATH,
                               TEXT_MATRIX_PATH,
                               MAX_NGRAMM,
                               MAX_FEATURES)
    src.merging_features(CLEANED_FEATURES_PATH,
                         TEXT_MATRIX_PATH,
                         FINISH_FEATURES)
