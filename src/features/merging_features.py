import pandas as pd
import scipy.sparse as sp
import click
from save_load_sparse_csr import load_sparse_csr, save_sparse_csr


@click.command()
@click.argument("input_path_df", type=click.Path(exists=True))
@click.argument("input_path_text_matrix", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def merging_features(input_path_df: str, input_path_text_matrix: str, output_path: str):
    """Function for merging digital and sparse text matrix
    :param input_path_df: Path to read original DataFrame
    :param input_path_text_matrix: Path to read text corpus (.npz)
    :param output_path: Path to write merging matrix (.npz)
    :return:
    """
    df = pd.read_csv(input_path_df)
    sparse_matrix = load_sparse_csr(input_path_text_matrix)
    df = df.drop(['Mechanics', 'Domains'], axis=1)
    output_matrix = sp.hstack((df, sparse_matrix), format='csr')
    save_sparse_csr(output_path, output_matrix)


if __name__ == "__main__":
    merging_features()
