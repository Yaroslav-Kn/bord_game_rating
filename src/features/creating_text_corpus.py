import pandas as pd
import nltk
from nltk.stem import WordNetLemmatizer
import re
import click

nltk.download('wordnet')
nltk.download('omw-1.4')
nltk.download('punkt')
lemmatizer = WordNetLemmatizer()


def clear_text(text):
    new_text = re.sub(pattern=r'[^a-zA-Z]',
                      repl=' ',
                      string=str(text))
    new_text = new_text.split()
    return ' '.join(new_text)


def lemmatize(text):
    text_list = nltk.word_tokenize(text)
    lemm_list = []
    for word in text_list:
        lemm_list.append(lemmatizer.lemmatize(word))
    lemm_text = " ".join(lemm_list)
    return lemm_text


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def creating_text_corpus(input_path: str, output_path: str):
    """Function for creating text corpus file
    :param input_path: Path to read original DataFrame
    :param output_path: Path to write text corpus (.txt)
    :return:
    """
    df = pd.read_csv(input_path)
    df['text'] = df['Mechanics'] + ' ' + df['Domains']

    df['lemm_text'] = df['text'].apply(clear_text)
    df['lemm_text'] = df['lemm_text'].apply(lemmatize)

    df['lemm_text'].to_csv(output_path,
                           sep=' ',
                           index=None,
                           header=None)


if __name__ == "__main__":
    creating_text_corpus()
