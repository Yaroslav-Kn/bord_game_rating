import pandas as pd
import click
from sklearn.model_selection import train_test_split
from save_load_sparse_csr import load_sparse_csr, save_sparse_csr

RANDOM_STATE = 123


@click.command()
@click.argument("input_path_features", type=click.Path(exists=True))
@click.argument("input_path_target", type=click.Path(exists=True))
@click.argument("output_path_features", type=click.Path(), nargs=2)
@click.argument("output_path_target", type=click.Path(), nargs=2)
def merging_features(input_path_features: str,
                     input_path_target: str,
                     output_path_features: list[str],
                     output_path_target: list[str]):
    """Function for merging digital and sparse text matrix
    :param input_path_features: Path to read features matrix (.npz)
    :param input_path_target: Path to read DF target
    :param output_path_features: List of path to write features
    :param output_path_target: List of path to write DF target
    :return:
    """
    features_matrix = load_sparse_csr(input_path_features)
    target = pd.read_csv(input_path_target)
    X_train, X_test, y_train, y_test = train_test_split(features_matrix,
                                                        target,
                                                        random_state=RANDOM_STATE,
                                                        test_size=0.25)
    save_sparse_csr(output_path_features[0], X_train)
    save_sparse_csr(output_path_features[1], X_test)
    y_train.to_csv(output_path_target[0], index=False)
    y_test.to_csv(output_path_target[1], index=False)


if __name__ == "__main__":
    merging_features()
